let number = Number(prompt("Give me a number"));
console.log("Chosen Number: " + number);

for(number = number; number > 50; number--){
	if(number % 10 === 0){
	console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	
	if (number % 5 === 0) {
		console.log(number);
		continue;
	}
	if(number <= 50){
		break;
	}
}

console.log("The current value is 50. Terminating the loop.");


let string = 'supercalifragilisticexpialidocious';

console.log(string);

let consonants = '';

for (let i = 0; i < string.length; i++){
	if (
			 string[i].toLowerCase() == "a" ||
			 string[i].toLowerCase() == "e" ||
			 string[i].toLowerCase() == "i" ||
			 string[i].toLowerCase() == "o" ||
			 string[i].toLowerCase() == "u"
		){
		continue;
		}
	else {
		consonants = consonants + string[i];
	}
}

console.log(consonants);
